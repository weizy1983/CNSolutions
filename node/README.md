Nodejs是现代JavaScript语言产生革命性变化的一个主要框架，它使得JavaScript从一门浏览器语言成为可以在服务器端运行、开发各种各样应用的通用语言。在不同的平台下，Nodejs的安装和配置也各有不同。由于Nodejs版本更迭频繁的特点（这一点和Python非常类似，也是这种充满活力的语言的共同特点），用户的不同项目可能在不同版本的Nodejs下开发进行（这些版本可能由于对第三方库版本依赖等不同原因而无法互相兼容），因此，在开发环境下一般建议通过安装版本管理器NVM （ Node Version Manager）来进行多版本的安装和使用。

本文介绍了开发环境与生产环境下安装并配置node的方法，开发环境下，Ubuntu 20.04下安装nvm的方法，Windows系统下使用[nvm-windows](https://github.com/coreybutler/nvm-windows)安装并管理node包的方法；生产环境下，可以使用预编译的二进制文件直接安装，或者使用docker进行安装与配置。

# 一、Ubuntu 20.04环境下安装与配置nodejs

## 预备

本方法需要用到wget或curl，以及nano或者vim进行文件编辑。需要先执行以下命令进行安装。在进行安装前，可以将ubuntu仓库配置为国内如清华镜像以提高速度。

```bash
sudo apt-get update
sudo apt-get install wget curl nano vim -y
```

> 在nano中，使用快捷键`Ctrl+O`，保存文件并退出，或者使用`Ctrl+X`，根据需要选择Y或者N来保存修改或者取消修改。

## **1. 开发环境下，通过NVM安装与配置Nodejs**

### 1. 1 更新github网站地址以进行nvm下载

NVM项目地址在github上，由于国内网络环境的原因，直接下载github相关资源比较困难。这需要依赖一个叫github520的开源项目，可以访问该项目在gitee上的[国内镜像](https://gitee.com/weizy1983/GitHub520)。将该项目的hosts文件添加到`/etc/hosts`文件中。
运行`sudo nano /etc/hosts`，该文件看上去可能像这样：
```bash
127.0.0.1       localhost
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
172.17.0.2      90bf9946446b
```
将`github520`项目中[`hosts`](https://gitee.com/weizy1983/GitHub520/raw/main/hosts)文件内容添加到该文件中后，运行`source /etc/hosts`，可以正常执行以下步骤。

### 1.2 在命令行通过curl或wget命令安装NVM（以下命令二选一）：
```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
```
如果上述命令无法下载，可以尝试使用以下命令之一访问该文件的国内镜像版本进行安装：
```bash
curl -o- https://gitee.com/weizy1983/CNSolutions/raw/master/node/nvm/install.sh | bash
wget -qO-  https://gitee.com/weizy1983/CNSolutions/raw/master/node/nvm/install.sh | bash
```

上述脚本会把NVM下载到 ~/.nvm文件夹下并将下面脚本添加到终端的配置文件中，根据使用的终端不同，配置文件可能是下面文件的某一个(~/.bash_profile,~/.zshrc.~/.profile或者~/.bashrc)，关闭终端并再次运行后可以使用nvm，也可以根据安装提示，将以下命令粘贴到命令行中，可以立即使用nvm命令。

```shell
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
```

###  1.3  修改环境变量，将nvm的node库替换为阿里镜像

nvm默认使用node全球镜像进行安装不同版本的node。需要在`/etc/profile`文件最后添加下面一行。运行`sudo nano /etc/profile`并将下面一行粘贴到最后。
```bash
export NVM_NODEJS_ORG_MIRROR=https://mirrors.tuna.tsinghua.edu.cn/nodejs-release/
```

### 1.4 使用NVM版本管理器安装并使用指定版本的node

```bash
#安装v16.13.2版本
nvm install v16.13.2
#配置v16.13.2为默认版本
nvm alias default v16.13.2
#使用v16.13.2
nvm use default
```

其他常用的nvm命令包括以下内容，也可以在命令行中仅输入nvm来查看帮助，可以登录[nodejs官网](https://nodejs.org/zh-cn/download/releases/)查看可以用的nodejs版本:

```bash
nvm current #查看当前正在使用的node版本
nvm ls #列出本地已安装的node版本
nvm ls-remote #列出远程所有版本可通过 |grep v12.* linux管道命令来进行筛选
nvm install v12 #安装v12版本的nodejs，如果不指定具体版本，默认安装当前大版本的LTS版
nvm install --lts #安装lts版本node
nvm uninstall --lts #删除某个node的lts版本
nvm use v12 #使用v12版本的nodejs
nvm use system #如果操作系统中已经安装有nvm可识别的nodejs，该命令会使用系统中的nodejs
nvm alias default v12 #设置操作系统别名，该命令将v12别名设置为default，可通过nvm use defaul使用
```
通过nvm安装并管理的各个不同版本，相互之间是各自独立的，因此，在一个版本中安装的全局安装包，并不会影响其他版本。这也正是nvm的优势所在。安装完node后，为提高npm或yarn包管理器下载速度，可以修改包仓库为淘宝镜像。
> 注意，由于nvm安装的各个node版本相互独立。因此，下一节的命令需要在每个版本的node下分别运行，才能分别设置仓库镜像。

### 1.5 配置npm和yarn的仓库为淘宝镜像

```bash
#配置npm仓库为淘宝镜像
npm config set registry https://registry.npm.taobao.org
npm config get registry
#全局安装yarn并配置yarn仓库为淘宝镜像
npm install -g yarn
yarn config set registry https://registry.npm.taobao.org
yarn config get registry
```


## **2. 生产环境下，使用预编译的安装包安装nodejs**

在用于生产环境的电脑上，一般仅需要运行一个特定版本的nodejs并配置相应环境，这时就不需要通过nvm包管理器进行管理与安装，只需要下载需要的版本进行安装与配置即可。推荐从国内镜像站点下载预编译的文件。如清华大学开源镜像站。以下为几个常用的版本：
- [V16.13.2 Linux X64](https://mirrors.tuna.tsinghua.edu.cn/nodejs-release/v16.13.2/node-v16.13.2-linux-x64.tar.xz)
- [v8.16.1 linux x64](https://mirrors.tuna.tsinghua.edu.cn/nodejs-release/v8.16.1/node-v8.16.1-linux-x64.tar.xz)
- [v12.14.0 linux x64(稳定推荐）](https://mirrors.tuna.tsinghua.edu.cn/nodejs-release/v12.14.0/node-v12.14.0-linux-x64.tar.xz)

使用`wget`或者`curl`下载源码并解压缩,将解压缩后的文件移动到常用的安装文件夹并分别为node，npm和npx创建软链接。
```bash
#下载V16.13.2 X64安装包
wget https://mirrors.tuna.tsinghua.edu.cn/nodejs-release/v16.13.2/node-v16.13.2-linux-x64.tar.xz
#解压缩下载的安装包
tar xvJf  node-v16.13.2-linux-x64.tar.xz
#修改文件夹名称为node
mv node-v16.13.2-linux-x64.tar.xz node
#将安装包移动到需要安装的目录下
sudo mv node /opt/node 
#打开/etc/profile文件
sudo nano /etc/profile
```

将以下内容添加到该文件最后部分
```bash
sudo ln -s /usr/node/bin/node /usr/local/bin/
sudo ln -s /usr/node/bin/npm /usr/local/bin/
sudo ln -s /usr/node/bin/npx /usr/local/bin/ 
```

输入`source /etc/profile`更新profile文件，输入`node -v`，`npm -v`应该可以看到node和npm的版本信息。安装成功，可参考1.5节安装并配置npm和yarn的镜像为阿里源。

## **3. 生产环境下，使用Docker安装并运行nodejs**

### 1. 镜像下载和安装
Docker是当下使用最广泛的容器产品，非常适合在运行环境下灵活部署。nodejs在各个版本在dokerhub上都有不同的映像，可以查看[dockerhub上nodejs地址](https://registry.hub.docker.com/_/node?tab=tags)了解并下载需要的镜像。如：
```bash
#下载基于alpine linux的node16.13.2版本
docker pull node:16.13.2-alpine
```
使用该镜像直接运行会进入node命令行（node的交互式终端）:
```bash
docker run -it --name mynode node:16.13.2-alpine
docker exec -it mynode node
```
在交互式命令行可以直接输入JavaScript语句执行，输入`.exit`可以退出终端。直接在node命令行执行JavaScript语句只能作为简单测试用，不能满足生产环境下部署项目的用途。要进行项目部署或者运行指定的文件，可以通过命令行、Dockerfile和docker-compose几种方式。

### 2. 示例项目

为了方便示范，本文新建一个简单的JavaScript文件`log.js`和一个简单的基于express的服务器项目。`log.js`文件内容如下，该文件不需要依赖任何第三方包，可以直接通过node直接运行：
```javascript
//log.js
console.log('hello world')
```
另外，我们需要新建一个简单的`server.js`文件，实现最基本的基于express的服务器。
```javascript
//server.js
'use strict';

const express = require('express');

// Constants
const PORT = 4000;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
  res.send('Hello world\n');
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
```
为了让该项目运行，我们还需要新建一个`package.json`文件。
```json
{
  "name": "docker_web_app",
  "version": "1.0.0",
  "description": "Node.js on Docker",
  "author": "First Last <first.last@example.com>",
  "main": "server.js",
  "scripts": {
    "start": "node server.js"
  },
  "dependencies": {
    "express": "^4.16.1"
  }
}
```
作为测试，我们可以在命令行运行`npm i`安装需要的包，然后运行`npm start`启动程序，在浏览器中输入`http://127.0.0.1:4000`可以看到`hello world`字样，服务器已成功运行。

### 3. 通过命令行启动docker nodejs运行javascript程序

- **直接运行**
如果只是运行简单的Javascript脚本。例如，直接运行`log.js`文件:
在当前目录下运行以下命令：
```bash
docker run -it --rm --name my-running-script -v "$PWD":/usr/src/app -w /usr/src/app node:16.13.2-alpine log.js
#widnows powershell下执行以下命令
docker run -it --rm --name my-running-script -v ${PWD}:/usr/src/app -w /usr/src/app node:16.13.2-alpine log.js
```
>说明：
>--rm 表示运行完成后删除当前容器 -v， 
> "\$PWD"将当前目录映射到容器中，
>  -w指定容器的工作目录
>如果是在windows的powershell下运行程序，需要将 "\$PWD"替换为\${PWD}才能运行

- **包含依赖时的运行**
上述命令执行后，会输出`hello world`，运行完成后自动销毁容器。如果要运行上述express项目，需要在进入容器前先使用同样版本的node运行`npm i`，安装所需的依赖包后，再运行以下命令。

```bash
docker run -itd --rm -p 18080:4000 --name my-running-script -v "$PWD":/usr/src/app -w /usr/src/app node:16.13.2-alpine server.js
```
>说明：
>和上一个操作略有不同，这里使用-itd让服务的命令行处于后台运行，如果去掉参数 d，在容器运行后可以看到终端输出 `Running on http://0.0.0.0:4000`。
>-p 18080:4000 将容器的4000端口映射到宿主机的18080，因此，需要在浏览器输入`http://127.0.0.1:18080`才能访问服务器

在终端输入`docker stop my-running-script`会停止并销毁容器，如果不需要容器在停止后销毁，则在输入命令时需要去掉`--rm`参数。

- **非root用户和终端无响应问题**
node默认推荐使用非root用户运行，但是docker的linux镜像默认使用root用户，为解决这一问题，docker的node镜像内置了一个非root用户node，**可以通过 `-u node`参数使用node用户执行命令,建议在使用Docker node镜像时使用node用户以避免遇到意外的问题**，例如：
```bash
docker run -it -u node --rm --name my-running-script -v "$PWD":/usr/src/app -w /usr/src/app node:16.13.2-alpine log.js
```

由于linux镜像默认使用pid为1的终端，导致在node脚本运行后无法响应系统中断等信号(例如通过`Ctrl+C`停止脚本的命令无效，只能通过外部`docker stop`命令停止容器)，加入`--init`参数可以解决这一问题，例如：
```bash
docker run -it --rm -p 18080:4000 --init --name my-running-script -v "$PWD":/usr/src/app -w /usr/src/app node:16.13.2-alpine server.js
```

### 4. 通过Dockerfile运行JavaScript程序
一般来说，命令行方式仅适合运行简单的docker命令，不适合在正式部署时使用，Dockerfile是一种常用的生成与部署基于Docker项目的工作方式，在上述示例项目根目录下建立一个名称为`Dockerfile`的文件如下：
```Dockerfile
# Node server
FROM node:16.13.2-alpine as node-server
WORKDIR /usr/src/app
COPY ["package.json", "./"]
RUN npm install --production --silent && mv node_modules ../
COPY server.js .

#使用非root用户
USER node
EXPOSE 4000
# CMD ["node", "server.js"]
CMD ["npm", "start"]
```
该文件规定了Dockers镜像的基础，运行需要的必要命令等。
在根目录下运行以下命令新建镜像：
```bash
docker build -t my-nodejs-server .
```
运行以下命令，在生成的镜像基础上建立容器并运行：
```bash
docker run -it --rm --name -p 18080:4000 my-running-server my-nodejs-server
```

### 5. 通过`docker-compose`方式运行JavaScript程序
`docker-compose`是另一种流行的建立docker文件的方式，相比Dockerfile来说，docker-compose更灵活，文件可读性更好，在项目根目录下新建`docker-compose.yml`文件如下：
```bash
version: "2"
services:
  node:
    image: "node:16.13.2-alpine"
    user: "node"
    working_dir: /home/node/app
    environment:
      - NODE_ENV=production
    volumes:
      - ./:/home/node/app
    ports:
      - 18080:4000
    command: "npm start"
```
在项目根目录下运行以下命令生成docker容器并运行。
```bash
docker-compose up -d
```
>说明:
>`docker-compose up`用于生成容器并运行，-d参数表示后台运行
>`docker-compose ps`用户列出目前所有容器
>`docker-compose stop`用于停止当前容器
>`docker-compose down`可以移除容器，也可以使用`docker container rm`命令移除
>`docker-compose -h`用于查看帮助文件
>`docker-compose build`用于重新构建项目中的容器

# 二、Windows 10 / Windows 11环境下安装与配置nodejs

## 预备

在Windows 10/Windows 11下，可以通过`wsl`（windows内置的linux系统）安装Ubuntu等linux版本进行项目开发，wsl安装与配置参见文章[Windows 10安装与管理WSL原生Linux系统](https://www.weizhiyong.com/2022/01/17/windows-10%e5%ae%89%e8%a3%85%e4%b8%8e%e7%ae%a1%e7%90%86wsl%e5%8e%9f%e7%94%9flinux%e7%b3%bb%e7%bb%9f/)及[Windows10安装与配置WSL2及基于WSL2的Docker环境](https://www.weizhiyong.com/2022/01/17/windows10%e5%ae%89%e8%a3%85%e4%b8%8e%e9%85%8d%e7%bd%aewsl2%e5%8f%8a%e5%9f%ba%e4%ba%8ewsl2%e7%9a%84docker%e7%8e%af%e5%a2%83/)。WSL与原生linux系统操作使用情况类似，在WSL下进行node安装与配置与前节相同。本节后续仅就windows原生系统下安装与配置nodejs进行介绍。

## 1.开发环境下使用nvm-windows安装与配置nodejs

### 1.1 NVM-Windows下载与安装

在NVM-windows版本发布页下载安装包。该项目维护和更新较慢，目前最新版本是1.1.9版，也可通过本[备用链接](https://gitee.com/weizy1983/CNSolutions/raw/master/node/nvm-windows/nvm-windows-setup-119.zip)进行下载。

如果下载了免安装版本，在使用前需要进行配置。如果下载的是nvm-setup安装版，则按照常规的windows安装程序进行安装即可。需要注意的是，nvm-windows默认安装在   C:\Users\[用户名]\AppData\Roaming\nvm 目录下， 在安装nvm for windows之前，需要卸载任何现有版本的node.js。并且需要删除现有的nodejs安装目录和npm安装目录（例如："C:\Program Files\nodejs’，“C:\Users\weiqinl\AppData\Roaming\npm”）。因为，nvm生成的symlink（符号链接/超链接)不会覆盖现有安装目录。

### 1.2 nvm-windows配置

 安装完成后，在命令行输入nvm可看到命令行提示，注意nvm-windows和nvm的命令不尽相同，熟悉nvm的用户可能需要习惯一下。nvm-windows配置文件可以通过命令行进行，也可以在安装目录下直接编辑 settings.txt 实现。

在 settings.txt 文件中修改下列行，可以配置nvm仓库为国内仓库，以加快node版本的下载和安装速度。
```text
node_mirror: https://npm.taobao.org/mirrors/node/
npm_mirror: https://npm.taobao.org/mirrors/npm/
```

### 1.3 nvm-windows常用命令
```bash
nvm list #查看本地所有node版本
nvm install 16.13.2 #安装16.13.2 版本
nvm use 16.13.2 #切换至 16.13.2 版本
nvm uninstall 16.13.2 #卸载16.13.2 版本
```

### 1.4 配置npm和yarn的仓库为淘宝镜像

```bash
#配置npm仓库为淘宝镜像
npm config set registry https://registry.npm.taobao.org
npm config get registry
#全局安装yarn并配置yarn仓库为淘宝镜像
npm install -g yarn
yarn config set registry https://registry.npm.taobao.org
yarn config get registry
```

## 2.生产环境下安装nodejs二进制文件

在[nodejs官网](https://nodejs.org/zh-cn/)下载需要版本的nodejs并进行安装后即可使用，目前最新的长期维护(LTS)版本为 [16.13.2](https://nodejs.org/dist/v16.13.2/node-v16.13.2-x64.msi)。也可以通过其他下载链接获取需要的版本。下载后可以通过前节说明更换更新源。

## 3.生产环境下通过Docker安装并配置nodejs

本部分与linux系统下操作基本相同，可参见本文相关部分。


## 参考资料
[docker-node官方仓库](https://github.com/nodejs/docker-node)

[Docker and Node.js Best Practices](https://github.com/nodejs/docker-node/blob/main/docs/BestPractices.md#global-npm-dependencies)

[原文链接](https://www.weizhiyong.com/2022/01/17/%e5%9c%a8linux%e4%b8%8ewindows%e5%bc%80%e5%8f%91%e4%b8%8e%e7%94%9f%e4%ba%a7%e7%8e%af%e5%a2%83%e4%b8%8b%e5%ae%89%e8%a3%85%e4%b8%8e%e9%85%8d%e7%bd%aenodejs/)