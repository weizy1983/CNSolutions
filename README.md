# CNSolutions

## 1. 介绍
由于网络环境原因，软件开发需要的部分文件很难下载，本仓库提供了一些与ubuntu、node、python等有关的国内镜像及文件等

## 2. 内容

1. node安装与运行环境需要的部分包及操作指南
2. ubuntu操作系统有关的部分包及操作指南
3. 国内环境无法访问github的解决方案，参见本仓库[Github520](https://gitee.com/weizy1983/GitHub520)

## 3. 使用说明

### 3.1 整体说明
1. 各子部分功能参见各文件夹内readme等文件
2. 参见[作者技术博客](https://www.weizhiyong.com)

### 3.2 Ubuntu更换清华源

安装好Ubuntu20.04系统后，下载[UbuntuTunaSource.sh](/ubuntu/tunasource/UbuntuTunaSource.sh)文件并执行。该脚本文件需要有执行权限，需要有sudo权限。
```bash
#下载脚本
wget https://gitee.com/weizy1983/CNSolutions/raw/master/ubuntu/tunasource/UbuntuTunaSource.sh
#修改脚本执行权限
sudo chmod 777 UbuntuTunaSource.sh
#执行脚本
#此处需要输入ubuntu版本信息，如果不输入，默认为20.04，目前支持的版本包括ubuntu16.04,ubuntu18.04,ubuntu20.04,ubuntu210.4,ubuntu21.10
sudo UbuntuTunaSource.sh ubuntu20.04
```

### 3.3 通过NVM安装管理Nodejs并更换源仓库

使用方法[参见本文](/node/README.md)

## 4. 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


## 5. 作者信息


魏智勇，80后，个人网站 [https://www.weizhiyong.com](https://www.weizhiyong.com)

维思团队网站 [https://www.wesinx.com](https://www.wesinx.com)
